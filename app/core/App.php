<?php

class App {
    public function parsingUrl($uri = '') {
        $uri = rtrim($uri, '/');
        $uri = filter_var ($uri, FILTER_SANITIZE_URL);
        $params = explode('/', $uri);
        return $params;
    }

    public function __construct() {
        $params = $this -> parsingUrl($_SERVER["REQUEST_URI"]);
        var_dump ($params);
    }
}

?>